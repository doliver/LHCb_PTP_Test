
#include "Barrel-Shifter.h"
#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>




void *run_thread(void *threadarg) //used for multithreading
{
    connection *con = (connection *) threadarg; //copy connection
    chrono::microseconds t(tnum);
    time_t tnow = chrono::system_clock::to_time_t(chrono::system_clock::now());
    tm *date = std::localtime(&tnow);
    date->tm_sec = 0;
    date->tm_min++;
    chrono::system_clock::time_point time = chrono::system_clock::from_time_t(mktime(date));
    
    int n=0;
    time=time + (t*(place-1));
    
    while (chrono::system_clock::now()<time); //wait for a whole minute so start in a synchronised way
    while (1)
    {
        if(!con->send_data(time+t)) break;
        time=time + (t*m_num);  //wait for next time slot
        while (chrono::system_clock::now()<time);
    }
    pthread_exit(NULL); //close thread
    
}


int main(int argc, char *argv[])
{
    if(argc != 5)
    {
        fprintf(stderr,"usage: server_name port_number time_period(microseconds) place(1,2,3,..)\n");
        exit(1);
    }
    
    //fork to 8 connections to increase throughput
    fork();
    fork();
    fork();
    
    connection  *con;   //connection object
    con = new connection(argv[1], argv[2]); //sets up connection
    tnum = atoi(argv[3]);
    chrono::microseconds t(tnum); //number of microseconds per pulse
    place = atoi(argv[4]); //sending position relative to the beginning of a second
    pthread_t threads[NUM_THREADS];
    int rc, i;
    char buffer[8];
    chrono::microseconds tbreak(10000);
    
    
    
    time_t tnow = chrono::system_clock::to_time_t(chrono::system_clock::now());
    tm *date = std::localtime(&tnow);
    date->tm_sec = 0;
    date->tm_min++;
    chrono::system_clock::time_point time = chrono::system_clock::from_time_t(mktime(date));
    chrono::system_clock::time_point temp;
    chrono::seconds one_second(1);
    
    int n=0;
    time=time + (t*(place-1));
    
    for(i=0; i < NUM_THREADS; i++) //create threads
    {
        rc = pthread_create(&threads[i], NULL,
                            run_thread, (void *)&con);
        if (rc){
            cout << "Error:unable to create thread," << rc << endl;
        }
    }

    
    while (chrono::system_clock::now()<time); //wait for a whole minute so start in a synchronised way
    fprintf(stderr,"Starting broadcast\n");
    while (1)
    {
        if(!con->send_data(time+t-tbreak)) break;
        time=time + (t*m_num);  //wait for next time slot
        while (chrono::system_clock::now()<time);
    }
    con->close_connection();
    fprintf(stderr,"Connection closed\n");
}


void connection::close_connection() //close the connection
{
    close(sockfd);
    freeaddrinfo(servinfo); // free the linked-list
}

connection::connection(char *server_name, char *portname) //initiates connection
{
    int status;
    memset(&hints, 0, sizeof hints); // make sure the struct is empty
    hints.ai_family = AF_UNSPEC;     // don't care IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
    hints.ai_flags = AI_PASSIVE;     // fill in my IP for me
    
    if ((status = getaddrinfo(server_name, portname, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        exit(1);
    }
    
    // make a socket:
    sockfd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    //fcntl(sockfd, F_SETFL, O_NONBLOCK);
    int flag = 1;
    setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(flag));
    
    //connect
    connect(sockfd, servinfo->ai_addr, servinfo->ai_addrlen);
    
    num=0;
    
    memset(&msg, 0, sizeof msg);
    
}

bool connection::send_data(chrono::system_clock::time_point t)
{
    int n_sent = 0;
    //fprintf(stderr,"H");
    //send(sockfd, , 128, 0);
    while(chrono::system_clock::now() < t)
    {
        //*((unsigned long*)&msg) = num;  //set the data to be the packet number
        if (send(sockfd, msg, MSG_SIZE, MSG_NOSIGNAL) < 0) //no signal flag so doesn't wait for network card if blocked
        {
            if (errno != EAGAIN || errno != EWOULDBLOCK) return 0; //error handling, return 0 rather than just killing the process
            //num--;
        }
        //num++;
        //for (int n = 0; n<100000; n++); //short pause (I think it is overflowing network card capacity)
    }
    return 1;
    //send(sockfd, * ( char *) &num, 1024, 0);
    
}
