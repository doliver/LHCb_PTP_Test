#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <chrono>
#include <iostream>
#include <fstream>
#include <pthread.h>
#include <vector>

using namespace std;

#define MAXBUFLEN 128000 //size of data to be recieved in each read
#define STDIN 0

const int num_cons = 40; //number of connections listener supports

int end_day = 32; //when to automatically end
int end_hour = 10;
int end_minute = 0;

const short bins = 10; //size of bins in microseconds

bool end;

const int count_size = 1000000/bins;
unsigned long count[count_size][7];
vector<short> transfer;



// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa) //fills addr structure
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

chrono::microseconds microseconds_since_second() //returns number of microseconds since the last whole second
{
    chrono::system_clock::time_point now = chrono::system_clock::now();
    time_t tnow = std::chrono::system_clock::to_time_t(now);
    tm *date = std::localtime(&tnow);
    //date->tm_sec = 0;
    
    std::chrono::system_clock::time_point second = std::chrono::system_clock::from_time_t(std::mktime(date));
    
    return chrono::duration_cast<std::chrono::microseconds>(now-second);
}

void *run_thread(void *threadarg) //for multithreading
{
    int sockfd = *(int *) threadarg; //copy socket
    char buf[MAXBUFLEN]; //create independent buffer
    int now, bytes;
    short i = transfer[sockfd];

    
    while (end) //read data
    {
        
        bytes = recv(sockfd,buf,MAXBUFLEN-1,0);
        now = microseconds_since_second().count();
        while (now<0 || now>999999) //check for invalid return from microseconds_since_second()
        {
            now = microseconds_since_second().count();
        }
        count[(now-now%bins)/bins][i]+=bytes;
        
    }
    
    close(sockfd);
    printf("Connection Closed\n");
    
    pthread_exit(NULL);
    
}


int main(int argc, char *argv[])
{
    
    if(argc != 2)
    {
        fprintf(stderr,"usage: port_number\n");
        exit(1);
    }
    int sockfd, newsockfd[num_cons];
    struct addrinfo hints, *servinfo, *p;
    int rv;
    int numbytes;
    struct sockaddr_storage their_addr;
    char buf[MAXBUFLEN];
    socklen_t addr_len;
    char s[INET6_ADDRSTRLEN];
    char *MYPORT = argv[1];
    end = 1;
    
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
    hints.ai_socktype = SOCK_STREAM; //TCP
    hints.ai_flags = AI_PASSIVE; // use my IP
    
    if ((rv = getaddrinfo(NULL, MYPORT, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }
    
    // loop through all the results and bind to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                             p->ai_protocol)) == -1) {
            perror("listener: socket");
            continue;
        }
        
        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("listener: bind");
            continue;
        }
        
        break;
    }
    
    if (p == NULL) {
        fprintf(stderr, "listener: failed to bind socket\n");
        return 2;
    }
    
    fd_set read_fds, master;
    int fdmax = 0;
    FD_ZERO(&read_fds);
    FD_ZERO(&master);
    
    FD_SET(STDIN, &read_fds);
    if (STDIN > fdmax) fdmax = STDIN;
    
    freeaddrinfo(servinfo);
    socklen_t sin_size;
    
    
    int i, j;
    
   //clear data structures
    memset(count, 0, sizeof(count));
    unsigned long drops[7];
    memset(drops, 0, sizeof(drops));
    unsigned long num[7];
    memset(num, 0, sizeof(num));
    int n = 0, now, rc;
    pthread_t threads[num_cons];
    
    
    time_t tnow = std::chrono::system_clock::to_time_t(chrono::system_clock::now());
    tm *time, *tstart;
    time = localtime(&tnow);
    tstart = new tm(*time);
    
    listen(sockfd,10);
    
    for (i = 0; i<num_cons; i++) //initiate connections by listening
    {
        sin_size = sizeof their_addr;
        if ((newsockfd[i] = accept(sockfd, (struct sockaddr *) &their_addr, &sin_size)) == -1) //accept new connection
        {
            perror("listener: accept");
            continue;
        }
        inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr *)&their_addr), s, sizeof s);
        fprintf(stderr, "Connected to %s: is %i\n", s, newsockfd[i]);
        const char *com = inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr*)&their_addr),s, sizeof s);
        
        //fills transfer structure which associates connections with ip addresses
        while (transfer.size() <= newsockfd[i]) transfer.push_back(6);
        
        if (*(com+10)=='2') transfer[newsockfd[i]]=0;    //lab09
        else if (*(com+10)=='1') transfer[newsockfd[i]]=1;   //lab10
        else if (*(com+10)=='5') transfer[newsockfd[i]]=2;   //lab11
        else if (*(com+10)=='6') transfer[newsockfd[i]]=3;   //lab12
        else if (*(com+10)=='3') transfer[newsockfd[i]]=4;   //lab13
        else if (*(com+10)=='4') transfer[newsockfd[i]]=5;   //lab15
        else transfer[newsockfd[i]]=6;
        
        rc = pthread_create(&threads[i], NULL,
                            run_thread, (void *)&newsockfd[i]); //create thread with the socket address
        if (rc){
            cout << "Error:unable to create thread," << rc << endl;
            //for (int j=0; j<NUM_THREADS; j++) pthread_cancel(threads[j]);
        }
    }
    
    FD_SET(STDIN,&read_fds); //for listening for CLI input
    
    master = read_fds;
    
    printf("listener: waiting to recv...\n");
    
    
    while (time->tm_mday < end_day || time->tm_hour < end_hour || time->tm_min < end_minute) //end=1 until end time is past or there is an input on the CLI
    {
        read_fds = master;
        if (select(1, &read_fds, NULL, NULL, NULL) == -1)
        {
            perror("select");
            exit;
        }
        if (FD_ISSET(STDIN, &read_fds))
        {
            end = 0;
            fprintf(stderr,"Closing Connections\n");
            break;
        }
    }
    end = 0; //for commmunicating with the threads, when =0 threads stop
    
    tnow = std::chrono::system_clock::to_time_t(chrono::system_clock::now());
    time = localtime(&tnow);
    
    fprintf(stderr,"Run time: %i seconds\n", ((((time->tm_mday - tstart->tm_mday)*24 + time->tm_hour - tstart->tm_hour)*60 + time->tm_min - tstart->tm_min - 1)*60) + time->tm_sec); //calculate run time based on start and end times

    //output data to file
    ofstream myfile;
    myfile.open ("data.txt");
    
    for (i=0; i<1000000/bins; i++)
    {
        myfile << count[i][0] << " " << count[i][1] << " " << count[i][2] << " " << count[i][3] << " " << count[i][4] << " " << count[i][5] << endl;
    }
    
    myfile.close();
    
    close(sockfd); //close sockets
    printf("Connection Closed\n");
    
    return 0;
}

