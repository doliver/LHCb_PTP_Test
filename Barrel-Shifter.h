#ifndef BARREL_SHIFTER_H_INCLUDED
#define BARREL_SHIFTER_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <chrono>
#include <signal.h>
#include <ctime>
#include <netinet/tcp.h>

using namespace std;

#define MSG_SIZE 128000 //number of Bytes per message
#define NUM_THREADS 0 //number of ADDITIONAL threads for each fork
int m_num = 5; //number of machines
int place;
int sockfd;
int tnum;
char msg[MSG_SIZE];
unsigned long num;
char buf[8];

class connection
{
private:

    struct addrinfo hints;
    struct addrinfo *servinfo;  // will point to the results
    

    
public:
    connection(char *server_name, char *portname);
    //Initialise connecting to a server port
    
public:
    bool send_data(chrono::system_clock::time_point t);
    //Sends random data at maximum bandwidth for time period t
    
    void close_connection();
    //Closes the connection to the server port of object
};





#endif
